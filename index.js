const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");
const app = express();
const PORT = 8000;
// get router
const router = require("./router");

app.use(cors());
app.use(express.json());
app.use(express.urlencoded({ extended: false }));

const dotenv = require("dotenv");
dotenv.config();

app.listen(PORT, async () => {
  console.log(`server up on port ${PORT}`);
});
// use router
app.use(router);

mongoose
  .connect(process.env.MONGODB_URL, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  })
  .then(() => {
    console.log("Connected to MongoDB");
  })
  .catch((err) => {
    console.log(err);
  });
